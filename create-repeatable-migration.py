#!/usr/bin/python
import sys

# Require at least one argument
if (len(sys.argv) < 2):
    print "You must provide at least one argument to describe the change"
    sys.exit()

# Final destination for alts
dest = "src/main/resources/db/migration"

filePath = dest + "/R" + "__" + "_".join(sys.argv[1:]) + ".sql"

f = open(filePath, 'a')
f.close()

print "Created migration template at"
print filePath

