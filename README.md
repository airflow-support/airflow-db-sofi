# This project contains all of the SQL DDL and DML for the Sofi Airflow project

## Creating New Database
Install postgres and run dropAndMigrate.sh - NOTE: this script logs into postgres with the "postgres" user.

## Steps for modifying the database

1. Run the script create-migration.py, which will create a properly named template in the src/main/resources/db/migration
directory

        $ ./create-migration.py <Ticket> foo more fooish
        Created migration template at
        src/main/resources/db/migration/V20110101080000__<Ticket>-foo-more-fooish.sql

2. Edit/Modify the script

        $ vi src/main/resources/db/migration/V20110101080000__<Ticket>-foo-more-fooish.sql
        [ ... ]

3. Verify the migration will run/verify correctness

        $ ./gradlew --no-daemon clean compileJava run -Pdb="localhost:5432/sofi_airflow" -Puser=airflow -Ppassword=sofi -Pcommand=version

4. Apply the new change to your local database (to verify your scripts will run properly)

        $ ./gradlew --no-daemon clean compileJava run -Pdb="localhost:5432/sofi_airflow" -Puser=airflow -Ppassword=sofi -Pcommand=migrate

5. Commit the test script to the repository on the appropriately named branch

        $ git add src/main/resources/db/migration/V20110101080000__<Ticket>-foo-more-fooish.sql
        $ git commit -m "Modified table 'foo' to be more fooish"

6. Create Pull Request
Make sure the merged branch is 'master' (not develop/migrations)

7. Wait for Approval and then Merge request to 'master'
Once the branch has been merged to 'migrations', the Bamboo server will then take the script
and rename it using the appropriate sequence number and then merge it to the 'develop' branch
with a new script name, such as 'V25.1__<Ticket>-foo-more-fooish.sql'.

## Error handling

If an error occurs in your script, you will have to 'repair' your database, and retry the migration
after correcting any errors in your script.

        $ ./gradlew --no-daemon clean compileJava run -Pdb="localhost:5432/sofi_airflow" -Puser=airflow -Ppassword=sofi -Pcommand=repair
        { Verify the database version }
        $ ./gradlew --no-daemon clean compileJava run -Pdb="localhost:5432/sofi_airflow" -Puser=airflow -Ppassword=sofi -Pcommand=version
        { Retry migration after correcting the script(s) }
        $ ./gradlew --no-daemon clean compileJava run -Pdb="localhost:5432/sofi_airflow" -Puser=airflow -Ppassword=sofi -Pcommand=migrate

## Local Docker Build

1. Build and test migration image

        $ ./docker.build

