#!/usr/bin/python
from datetime import datetime
from pytz import timezone
import sys

# Require at least one argument
if (len(sys.argv) < 2):
  print "You must provide at least one argument to describe the change"
  sys.exit()

# Date format for version numbers.
dateFmt = "%Y%m%d%H%M%S"
now = datetime.now(timezone("US/Eastern")).strftime(dateFmt)

# Final destination for alts
dest = "src/main/resources/db/migration"

filePath = dest + "/V" + now + "__" + "-".join(sys.argv[1:]) + ".sql"

f = open(filePath, 'a')
f.write("-- Use \"sofi.user\" for any created_by/updated_by fields\n\n")
f.write("-- Ex:\n")
f.write("-- set session \"sofi.user\" = 'automation@sofi.org';\n\n")
f.write("-- INSERT INTO foo (id,created_by,created_date) VALUES (1,current_setting('sofi.user'),NOW())\n")
f.write("-- Please remove above example lines before committing script\n\n")
f.write("-- Set the proper timezone (so NOW() is in the correct timezone)\n")
f.write("set session time zone 'America/New_York';\n\n")
f.close()

print "Created migration template at"
print filePath
