SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

-- Create the default schema
CREATE SCHEMA IF NOT EXISTS airflow AUTHORIZATION airflow;

-- Add permissions to the admin user and application role
GRANT ALL ON SCHEMA airflow TO airflow;
GRANT USAGE ON SCHEMA airflow TO airflowapprole;

--
-- Set default PRIVILEGES for airflow admin to delegate
-- rights to the application role.
--

-- Crud permissions
ALTER DEFAULT PRIVILEGES FOR ROLE airflow IN SCHEMA airflow
 GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO airflowapprole;

-- Sequence permissions
ALTER DEFAULT PRIVILEGES FOR ROLE airflow IN SCHEMA airflow
 GRANT SELECT, USAGE ON SEQUENCES TO airflowapprole;

-- Function/procedure permissions
ALTER DEFAULT PRIVILEGES FOR ROLE airflow IN SCHEMA airflow
 GRANT EXECUTE ON FUNCTIONS TO airflowapprole;
