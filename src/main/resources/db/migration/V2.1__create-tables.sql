-- TODO: Define tables here...

DROP TABLE IF EXISTS xxx;

CREATE TABLE xxx (
  id bigserial primary key,
  note text,
  created_dt timestamp without time zone NOT NULL DEFAULT timezone('utc'::text, now())
);
