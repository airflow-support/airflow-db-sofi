package com.sofi.db;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CoreMigrator extends Migrate {
    public static void main(String[] args)
      throws IOException {
        new CoreMigrator().run(args);
    }

    @Override
    protected String getConfig() {
        return "airflow-db.conf";
    }

    @Override
    protected String getDefaultSchemas() {
        return "airflow";
    }

    @Override
    protected DbType getDbType() {
        return DbType.POSTGRES;
    }

    @Override
    protected Map<String, String> getPlaceholders() {
        Map<String, String> params = new HashMap<>();
        params.put("sysemail", "'automation@sofi.org'");
        return params;
    }
}
