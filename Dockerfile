FROM registry.sofi.com/openjdk:12.0.1-jdk-centos-98 as java_builder

WORKDIR '/build'

## Stage Build Files (facilitate docker caching)
COPY gradlew settings.gradle.kts build.gradle.kts gradle.properties /build/
COPY gradle /build/gradle
RUN ./gradlew --build-cache --no-daemon --gradle-user-home=/build distTar

## Build Migration Package
COPY . '/build/'
RUN ./gradlew --build-cache --no-daemon --gradle-user-home=/build distTar
RUN mkdir -p /output && \
    tar -xf /build/build/distributions/airflow-db-1.0-SNAPSHOT.tar --strip-components=1 -C /output

## Create Migration Image
FROM registry.sofi.com/sofi-postgres-migrate

ENV APP_NAME airflow-db
ENV DB_NAME sofi_airflow
COPY --from=java_builder /output/ /data/sofi/

RUN apk update && apk upgrade

