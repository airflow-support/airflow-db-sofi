pluginManagement {
    repositories {
        gradlePluginPortal()
        maven { url = uri("https://build.sofi.com/artifactory/repo/") }
    }
}
rootProject.name = "airflow-db"

