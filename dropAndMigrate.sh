#!/bin/sh -e

# Defaults if no arguments are passed
DB_NAME=sofi_airflow
HOST=127.0.0.1
PORT=5432
USER=airflow
PASS=sofi

# Default root user
DB_ROOT_USER=postgres

# Handle command line arguments
while [ $# -gt 0 ]; do
  case "$1" in
  -d)
    DB_NAME="$2"
    shift
    ;;

  -h)
    HOST="$2"
    shift
    ;;

  -p)
    PORT="$2"
    shift
    ;;

  -U)
    USER="$2"
    shift
    ;;

  -P)
    PASS="$2"
    shift
    ;;

   *)
    # Script name
    script=`basename "$0"`
    echo "Usage: $script [-d dbname] [-h host] [-p port] [-U user] [-P password]"
    exit;
    ;;
  esac
  shift
done

# Do the deed
echo "dropping and creating db"; ./testDbSetup/resetDB.sh -h $HOST -p $PORT -U $DB_ROOT_USER -P $PASS
echo "running migrate"; ./gradlew --no-daemon clean compileJava run -Pdb="$HOST:$PORT/$DB_NAME" -Puser=$USER -Ppassword=$PASS -Pcommand=migrate
echo "running loadData script"; ./testDbSetup/loadData.sh -d $DB_NAME -h $HOST -p $PORT -U $USER -P $PASS
