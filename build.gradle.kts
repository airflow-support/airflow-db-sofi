plugins {
    application
    distribution
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

repositories {
    mavenCentral()
    maven("https://build.sofi.com/artifactory/repo/")
}

dependencies {
    implementation("com.sofi:data-utils:release-30")
    implementation("org.postgresql:postgresql:42.2.5")
}

application {
    mainClassName = "com.sofi.db.CoreMigrator"
}

tasks {
    "run"(JavaExec::class) {
        doFirst {
            (this as JavaExec).apply {
                val db = project.property("db")
                val user = project.property("user")
                val password = project.property("password")
                val command = project.property("command")
                args = listOf("$db", "$user", "$password", "$command")
            }
        }
    }
}

group = "com.sofi"

application {
    applicationDistribution.from("testDbSetup") {
        into("testDbSetup")
    }
}


