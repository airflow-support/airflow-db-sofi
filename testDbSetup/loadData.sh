#!/bin/sh -e

# Defaults if no arguments are passed
DB_NAME=sofi_airflow
HOST=127.0.0.1
PORT=5432
USER=airflow
PASS=sofi

# Handle command line arguments
while [ $# -gt 0 ]; do
  case "$1" in
  -d)
    DB_NAME="$2"
    shift
    ;;

  -h)
    HOST="$2"
    shift
    ;;

  -p)
    PORT="$2"
    shift
    ;;

  -U)
    USER="$2"
    shift
    ;;

  -P)
    PASS="$2"
    shift
    ;;

   *)
    # Script name
    script=`basename "$0"`
    echo "Usage: $script [-d dbname] [-h host] [-p port] [-U user] [-P password]"
    exit;
    ;;
  esac
  shift
done

# Move to the directory where the scripts are stored
cd "${0%/*}"

psqlcmd() {
    PGPASSWORD=$PASS psql -d $DB_NAME -h $HOST -p $PORT -U $USER -f $1
}

# TODO: Uncomment to load all the test data into the DB (assumes test_data.sql file exists).
#echo "loading test data"; psqlcmd test_data.sql
