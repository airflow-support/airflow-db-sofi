-- Kill existing connections
SELECT pg_terminate_backend(pg_stat_activity.pid)
  FROM pg_stat_activity
 WHERE pg_stat_activity.datname = 'sofi_airflow' AND
       pid <> pg_backend_pid();

-- Delete existing DB and users
DROP DATABASE IF EXISTS sofi_airflow;
DROP OWNED BY airflow;
DROP USER IF EXISTS airflow;

-- Create the admin user and then create the database and permissions
CREATE USER airflow WITH CREATEDB PASSWORD 'sofi' LOGIN;
CREATE DATABASE sofi_airflow WITH OWNER = airflow;
GRANT ALL PRIVILEGES ON DATABASE sofi_airflow TO airflow;

-- Create application user
DROP USER IF EXISTS airflowapp;
CREATE USER airflowapp WITH PASSWORD 'sofi';

-- Next, create the application role and grant it's permissions to the application user
DROP ROLE IF EXISTS airflowapprole;
CREATE ROLE airflowapprole;
GRANT airflowapprole to airflowapp;

-- Finally the search path for the (not-yet-created) schema
--
-- We must do the search path here since we can only alter
-- roles with superuser privs, and the remaining scripts
-- are run with reduced (airflow) privs.
ALTER USER airflow SET SEARCH_PATH = airflow, pg_catalog, public;
ALTER USER airflowapp SET SEARCH_PATH = airflow, pg_catalog, public;
ALTER USER postgres SET SEARCH_PATH = airflow, pg_catalog, public;
