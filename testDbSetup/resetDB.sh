#!/bin/sh -e

# Defaults if no arguments are passed
HOST=127.0.0.1
PORT=5432
USER=postgres
PASS=sofi
DB=postgres

# Handle command line arguments
while [ $# -gt 0 ]; do
  case "$1" in
  -h)
    HOST="$2"
    shift
    ;;

  -p)
    PORT="$2"
    shift
    ;;

  -U)
    USER="$2"
    shift
    ;;

  -P)
    PASS="$2"
    shift
    ;;

   *)
    # Script name
    script=`basename "$0"`
    echo "Usage: $script [-h host] [-p port] [-U user] [-P password]"
    exit;
    ;;
  esac
  shift
done

# Move to the directory where the scripts are stored
cd "${0%/*}"

psqlcmd() {
    PGPASSWORD=$PASS psql -h $HOST -p $PORT -U $USER -f $1 -d $2
}

echo "Rebuilding database"; psqlcmd dropAndCreate.sql postgres
echo "Adding extensions"; psqlcmd createExtensions.sql sofi_airflow
